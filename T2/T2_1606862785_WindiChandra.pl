no1();
no2();
no3();
no4();

sub no1 {
	open($input_file, '<', 'Korpus1.txt');
	open($output_file, '>', 'word_list_result.txt');

	%frequency = ();
	$is_text = 0;

	while ($row = <$input_file>) {
		chomp $row;
		# for windows platform
		if ($row =~ /\r$/) {
			chop $row;
		}

		if ($row eq "<TEXT>") {
			$is_text = 1;
		} elsif ($row eq "</TEXT>") {
			$is_text = 0;
		} elsif ($is_text == 1) {
			foreach $word (split(/[^a-zA-Z0-9-]/, $row)) {
				if ($word =~ /^[a-zA-Z-]+$/) {
					$frequency{lc($word)} += 1;
				}
			}
		}
	}

	for $word (keys %frequency) {
		print $output_file "$word\n";
	}

	close $input_file;
	close $output_file;
}

sub no2 {
	open($input_file, '<', 'gold_standard_stemming.txt');
	$res = 0;
	while ($row = <$input_file>) {
		chomp $row;
		# for windows platform
		if ($row =~ /\r$/) {
			chop $row;
		}
		@words = split(' ', $row);
		if (stemming($words[0]) eq $words[2]) {
			$res++;
		}

		# print stemming($row), "\n";
	}
	print $res, "\n";
	close $input_file;
}

sub no3 {
	open($input_file1, '<', 'word_list_result.txt');
	open($input_file2, '<', 'Korpus2.txt');
	open($output_file, '>', 'soundex_result.txt');

	# map soundex
	%soundex_map = ();
	while ($row = <$input_file1>) {
		chomp $row;
		# for windows platform
		if ($row =~ /\r$/) {
			chop $row;
		}
		if (exists $soundex_map{soundex($row)}) {
			push(@{$soundex_map{soundex($row)}}, $row);
		} else {
			$soundex_map{soundex($row)} = [$row];
		}
	}

	# spell correction
	%corrections = ();
	$is_text = 0;
	while ($row = <$input_file2>) {
		chomp $row;
		# for windows platform
		if ($row =~ /\r$/) {
			chop $row;
		}
		if ($row eq "<TEXT>") {
			$is_text = 1;
		} elsif ($row eq "</TEXT>") {
			$is_text = 0;
		} elsif ($is_text == 1) {
			foreach $word (split(/[^a-zA-Z0-9-]/, $row)) {
				if ($word =~ /^[a-zA-Z-]+$/) {
					if (exists $corrections{$word}) {
						next;
					}
					if (exists $soundex_map{soundex($word)}) {
						@corrections_list = @{$soundex_map{soundex($word)}};
						$correction = $corrections_list[int(rand(length(@corrections_list)))];
						$corrections{$word} = $correction;
					}
				}
			}
		}
	}

	for $correction (keys %corrections) {
		print $output_file $correction, " -> ", $corrections{$correction}, "\n";
	}

	close $input_file1;
	close $input_file2;
	close $output_file;
}

sub no4 {
	open($input_file1, '<', 'word_list_result.txt');
	open($input_file2, '<', 'Korpus2.txt');
	open($output_file, '>', 'edit_distance_result.txt');

	# map soundex
	%tokens_map = ();
	while ($row = <$input_file1>) {
		chomp $row;
		# for windows platform
		if ($row =~ /\r$/) {
			chop $row;
		}
		$tokens_map{$row} = 1;
	}

	@tokens_list = {};
	for $token (keys %tokens_map) {
		push(@tokens_list, $token);
	}

	# spell correction
	%corrections = ();
	while ($row = <$input_file2>) {
		chomp $row;
		# for windows platform
		if ($row =~ /\r$/) {
			chop $row;
		}
		if ($row eq "<TEXT>") {
			$is_text = 1;
		} elsif ($row eq "</TEXT>") {
			$is_text = 0;
		} elsif ($is_text == 1) {
			foreach $word (split(/[^a-zA-Z0-9-]/, $row)) {
				if ($word =~ /^[a-zA-Z-]+$/) {
					if (exists $tokens_map{$word} or exists $corrections{$word}) {
						next;
					}
					$minimum_distance = 1000;
					$correction = '';
					foreach $token (@tokens_list) {
						$current_distance = edit_distance($word, $token);
						if ($minimum_distance > $current_distance) {
							$minimum_distance = $current_distance;
							$correction = $token;
						}
					}
					print $word, " -> ", $correction, " ", $minimum_distance, "\n";
					$corrections{$word} = $correction;
				}
			}
		}
	}

	for $correction (keys %corrections) {
		print $output_file $correction, " -> ", $corrections{$correction}, "\n";
	}

	close $input_file1;
	close $input_file2;
	close $output_file;	
}

sub edit_distance {
	($word1, $word2) = @_;
	$n = length($word1);
	$m = length($word2);
	for ($i = 0; $i <= $n; $i++) {
		for ($j = 0; $j <= $m; $j++) {
			if ($i == 0) {
                $dp[$i][$j] = $j;
            } elsif ($j == 0) {
                $dp[$i][$j] = $i;
            } else {
            	$dp[$i][$j] = $dp[$i - 1][$j] + 1;
            	$dp[$i][$j] = min($dp[$i][$j], $dp[$i][$j - 1] + 1);

            	if (substr($word1, $i - 1, 1) eq substr($word2, $j - 1, 1)) {
                	$dp[$i][$j] = min($dp[$i][$j], $dp[$i - 1][$j - 1]);
            	} else {
               		$dp[$i][$j] = min($dp[$i][$j], $dp[$i - 1][$j - 1] + 1);
            	}
            	if ($i > 1 and $j > 1 and substr($word1, $i - 1, 1) eq substr($word2, $j - 2, 1) and substr($word1, $i - 2, 1) eq substr($word2, $j - 1, 1)) {
            		# swap
            		$dp[$i][$j] = min($dp[$i][$j], $dp[$i - 2][$j - 2] + 1);
            	}
            }
		}
	}
	return $dp[$n][$m];
}

sub min {
	($a, $b) = @_;
	if ($a < $b) {
		return $a;
	}

	return $b;
}

sub soundex {
	($word) = @_;
	$first_char = substr $word, 0, 1;
	$first_char = uc($first_char);
	$result = $word;
	$result =~ s/[aeiouyhw]/0/g;
	$result =~ s/[bfpv]/1/g;
	$result =~ s/[cgjkqsxz]/2/g;
	$result =~ s/[dt]/3/g;
	$result =~ s/[l]/4/g;
	$result =~ s/[mn]/5/g;
	$result =~ s/[r]/6/g;
	$result =~ s/(.)\1{1,}/\1/g;
	$result =~ s/[0]//g;
	$result =~ s/[-]//g;
	$result = $result . '000';
	$result = $first_char . substr($result, 0, 3);

	return $result;
}

sub is_vowel {
	($char) = @_;
	if ($char =~ /^[aeiou]/) {
		return 1;
	}
	return 0;
}

sub subs {
	($word, $prefix_length, $suffix_length) = @_;
	$result = substr $word, $prefix_length, -$suffix_length;

	# content sensitive rules check
	if (length($result) < 4) {
		return $word;
	}

	return $result;
}

sub remove_inflexional_particle {
	($word) = @_;
	@affixes = ('kah', 'lah', 'tah', 'pun');

	foreach $affix (@affixes) {
		if ($word =~ /$affix$/) {
			return subs($word, 0, length($affix));
		}
	}


	return $word;
}

sub remove_inflexional_possesive {
	($word) = @_;
	@affixes = ('ku', 'mu', 'nya');
	foreach $affix (@affixes) {
		if ($word =~ /$affix$/) {
			return subs($word, 0, length($affix));
		}
	}

	return $word;
}

sub replace {
	($word, $from, $to, $replacement) = @_;
	$left = substr $word, 0, $from - 1;
	if ($from == 0) {
		$left = '';
	}
	$right = substr $word, $to;
	return $left . $replacement . $right;
}

sub stemming {
	($word) = @_;
	# remove repeatance
	if ($word =~ /[a-z]-[a-z]/) {
		$pos = index $word, '-';
		$word = substr $word, 0, $pos;
	}

	# remove inflexionals
	$word = remove_inflexional_particle($word);
	$word = remove_inflexional_possesive($word);

	# remove suffix
	@suffixes = ('i', 'kan', 'an');
	$used_suffix = '';
	foreach $suffix (@suffixes) {
		if ($word =~ /$suffix$/) {
			$used_suffix = $suffix;
			$word = subs($word, 0, length($suffix));
			last;
		}
	}

	# remove prefix
	$counter = 0;
	$used_prefix = {};
	while ($counter < 3) {
		# constraint sensitive rule check
		if (length($word) <= 5) {
			last;
		}

		if ($word =~ /^me/) {
			if ($used_suffix eq 'kan' or $used_prefix{'me'}) {
				last;
			}

			$used_prefix{'me'} = 1;

			if ($word =~ /^mem/) {
				if (is_vowel(substr ($word, 3, 1))) {
					$word = replace($word, 0, 3, 'p');
				} else {
					$word = replace($word, 0, 3, '');
				}
				$counter++;
			} elsif ($word =~ /^meny/) {
				if (is_vowel(substr ($word, 3, 1))) {
					$word = replace($word, 0, 4, 's');
				} else {
					$word = replace($word, 0, 4, '');
				}
			} elsif ($word =~ /^meng/) {
				$word = replace($word, 0, 4, '');
			} elsif ($word =~ /^men/) {
				if (is_vowel(substr($word, 3, 1))) {
					$word = replace($word, 0, 3, 't');
				} else {
					$word = replace($word, 0, 3, '');
				}
			} else {
				$word = replace($word, 0, 2, '');
			}
		} elsif ($word =~ /^be/) {
			if ($used_suffix eq 'i' or $used_prefix{'be'}) {
				last;
			}
			if ($word =~ /^belajar/) {
				$word = replace($word, 0, 3, '');
				$used_prefix{'be'} = 1;
				$counter++;
			} elsif ($word =~ /^be.er/ and is_vowel(substr($word, 5, 1)) == 0) {
				$word = replace($word, 0, 2, '');
				$used_prefix{'be'} = 1;
				$counter++;
			} elsif ($word =~ /^ber/) {

				$word = replace($word, 0, 3, '');
				$used_prefix{'be'} = 1;
				$counter++;
			} else {
				last;
			}
		} elsif ($word =~ /^di/) {
			if ($used_suffix eq 'an' or $used_prefix{'di'}) {
				last;
			}
			$word = replace($word, 0, 2, '');
			$counter++;
			$used_prefix{'di'} = 1;
		} elsif ($word =~ /^ke/) {
			if ($used_suffix eq 'i' or $used_suffix eq 'kan' or $used_prefix{'ke'}) {
				last;
			}
			$word = replace($word, 0, 2, '');
			$counter++;
			$used_prefix{'ke'} = 1;
		} elsif ($word =~ /^se/) {
			if ($used_suffix eq 'i' or $used_suffix eq 'kan' or $used_prefix{'se'}) {
				last;
			}
			$word = replace($word, 0, 2, '');
			$counter++;
			$used_prefix{'se'} = 1;
		} elsif ($word =~ /^ter/) {
			if ($used_suffix eq 'an' or $used_prefix{'ter'}) {
				last;
			}
			$word = replace($word, 0, 3, '');
			$counter++;
			$used_prefix{'ter'} = 1;
		} elsif ($word =~ /^pe/) {
			if ($used_prefix{'pe'}) {
				last;
			}
			if ($word =~ /^pelajar/) {
				$word = replace($word, 0, 3, '');
			} elsif ($word =~ /^peng/) {
				$word = replace($word, 0, 4, '');
			} elsif ($word =~ /^per/ and is_vowel(substr($word, 3, 1)) == 0) {
				$word = replace($word, 0, 3, '');
			} elsif ($word =~ /^pem/) {
				if (is_vowel(substr ($word, 3, 1))) {
					$word = replace($word, 0, 3, 'p');
				} else {
					$word = replace($word, 0, 3, '');
				}
				$counter++;
			} elsif ($word =~ /^peny/) {
				if (is_vowel(substr ($word, 3, 1))) {
					$word = replace($word, 0, 4, 's');
				} else {
					$word = replace($word, 0, 4, '');
				}
			} elsif ($word =~ /^pen/) {
				if (is_vowel(substr($word, 3, 1))) {
					$word = replace($word, 0, 3, 't');
				} else {
					$word = replace($word, 0, 3, '');
				}
			} else {
				$word = replace($word, 0, 2, '');
			}
			$counter++;
			$used_prefix{'pe'} = 1;
		}
		else {
			last;
		}
	}

	for $pref (keys %used_prefix) {
		$used_prefix{$pref} = 0;
	}

	return $word;
}