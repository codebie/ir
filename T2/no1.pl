open($input_file, '<', 'Korpus1.txt');
open($output_file, '>', 'word_list_result.txt');

$frequency = {};
$is_text = 0;

while ($row = <$input_file>) {
	chomp $row;
	if ($row eq "<TEXT>") {
		$is_text = 1;
	} elsif ($row eq "</TEXT>") {
		$is_text = 0;
	} elsif ($is_text == 1) {
		foreach $word (split(/[^a-zA-Z0-9]/, $row)) {
			if ($word =~ /^[a-zA-Z]+$/) {
				$frequency{lc($word)} += 1;
			}
		}
	}
}

for $word (keys %frequency) {
	print $output_file "$word\n";
}

close $input_file;
close $output_file;