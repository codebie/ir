use lib 'lib/';
use Stemmer;

# subroutine to prepare word before further processing (include suffix and prefix symbols removal, lowercasing, stemming)
# @param $word: word to be preprocessed
# return preprocessed word
sub preprocess {
	($word) = @_;
	$word = lc($word);
	$n = length($word);

	# remove prefix symbols
	for ($i = 0; $i < $n; $i++) {
		if (substr($word, $i, $i + 1) =~ /[a-zA-Z]/) {
			$word = substr($word, $i);
			last;
		}
	}

	# remove suffix symbols
	for ($i = $n - 1; $i >= 0; $i--) {
		if (substr($word, $i, $i + 1) =~ /[a-zA-Z]/) {
			$word = substr($word, 0, $i + 1);
			last;
		}
	}

	return stem($word);
}

# subroutine to find intersected elements(integers) in two SORTED arrays
# @param $arr1_ref: a reference to first array
# @param $arr2_ref: a reference to second array
# return array containing all intersected elements
sub boolean_and {
	($arr1_ref, $arr2_ref) = @_;
	@arr1 = @{ $arr1_ref };
	@arr2 = @{ $arr2_ref };
	$n = @arr1;
	$m = @arr2;
	$p1 = 0;
	$p2 = 0;
	@result = ();
	while ($p1 < $n and $p2 < $m) {
		if ($arr1[$p1] == $arr2[$p2]) {
			push @result, $arr1[$p1];
			$p1++;
			$p2++;
		} elsif ($arr1[$p1] < $arr2[$p2]) {
			$p1++;
		} else {
			$p2++;
		}
	}

	return @result;
}

# subroutine to find union of elements(integers) in two SORTED arrays
# @param $arr1_ref: a reference to first array
# @param $arr2_ref: a reference to second array
# return array containing union of elements
sub boolean_or {
	($arr1_ref, $arr2_ref) = @_;
	@arr1 = @{ $arr1_ref };
	@arr2 = @{ $arr2_ref };
	$n = @arr1;
	$m = @arr2;
	$p1 = 0;
	$p2 = 0;
	@result = ();
	while ($p1 < $n or $p2 < $m) {
		if ($p1 == $n) {
			push @result, $arr2[$p2];
			$p2++;
		} elsif ($p2 == $m) {
			push @result, $arr1[$p1];
			$p1++;
		} elsif ($arr1[$p1] == $arr2[$p2]) {
			push @result, $arr1[$p1];
			$p1++;
			$p2++;
		} elsif ($arr1[$p1] < $arr2[$p2]) {
			push @result, $arr1[$p1];
			$p1++;
		} else {
			push @result, $arr2[$p2];
			$p2++;
		}
	}

	return @result;	
}

# subroutine to find list of documents containing a given word
# @param $word: the word to be looked up
# return list of documents containing the word
sub get_docs {
	($word) = @_;
	return sort {$a <=> $b} keys %{$inverted_index{$word}};
}

# subroutine to find list of documents NOT containing a given word
# @param $word: the word to be looked up
# return list of documents NOT containing the word
sub get_not_docs {
	($word) = @_;
	@result = ();
	for ($i = 1; $i <= $doc_number; $i++) {
		if ($inverted_index{$word}{$i}{$in_title} != 1 and $inverted_index{$word}{$i}{$in_text} != 1) {
			push @result, $i;
		}
	}
	return @result;
}

# subroutine to determine the score of each document from a set of keywords
# @param $docs_reference: reference to array of documents
# @param $words_reference: reference to array of words
# return a hash with document as key and the respective score as value
sub score {
	($docs_reference, $words_reference) = @_;
	@docs = @{ $docs_reference };
	@words = @{ $words_reference };
	%result = ();
	foreach $doc (@docs) {
		foreach $word (@words) {
			if ($inverted_index{$word}{$doc}{$in_title} == 1) {
				$result{$doc} += 0.7;
			}
			if ($inverted_index{$word}{$doc}{$in_text} == 1) {
				$result{$doc}  += 0.3;
			}
		}
	}
	return %result;
}

# subroutine to lookup documents fulfilling a proximity NEAR query
# @param $word1: the first word
# @param $word2: the second word
# @param $near: the NEAR parameter (allowing $near - 1 word(s) between $word1 and $word2)
# return a list of documents based on the query
sub boolean_near {
	($word1, $word2, $near) = @_;
	@result = ();
	for ($doc = 1; $doc <= $doc_number; $doc++) {
		$valid = 0;
		my @positions1 = sort {$a <=> $b} keys %{$inverted_index_pos{$word1}{$doc}};
		my @positions2 = sort {$a <=> $b} keys %{$inverted_index_pos{$word2}{$doc}};
		$n = @positions1;
		$m = @positions2;
		if ($n == 0 or $m == 0) {
			next;
		}
		$p1 = 0;
		for ($p2 = 0; $p2 < $m; $p2++) {
			while ($p1 < $n - 1 && $positions1[$p1 + 1] <= $positions2[$p2]) {
				$p1++;
			}
			if ($positions1[$p1] <= $positions2[$p2] and $positions2[$p2] - $positions1[$p1] <= $near and $inverted_index{$word1}{$doc}{$positions1{$p1}} == $inverted_index{$word2}{$doc}{$positions2{$p2}}) {
				$valid = 1;
				last;
			}
		}
		if ($valid == 1) {
			push @result, $doc;
		}
	}
	return @result;
}

# subroutine to lookup documents fulfilling a proximity query with format: $word1 NEAR $near ($word2 WITH $word3)
# @param $word1: the first word
# @param $word2: the second word
# @param $word3: the third word
# @param $near: the NEAR parameter (allowing $near - 1 word(s) between $word1 and $word2)
# return a list of documents based on the query
sub boolean_near_plus_with {
	($word1, $word2, $word3, $near) = @_;
	@result = ();
	for ($doc = 1; $doc <= $doc_number; $doc++) {
		$valid = 0;
		my @positions1 = sort {$a <=> $b} keys %{$inverted_index_pos{$word1}{$doc}};
		my @positions2 = sort {$a <=> $b} keys %{$inverted_index_pos{$word2}{$doc}};
		$n = @positions1;
		$m = @positions2;
		$p1 = 0;
		for ($p2 = 0; $p2 < $m; $p2++) {
			if ($inverted_index_pos{$word3}{$doc}{$positions2{$p2} + 1} != $inverted_index_pos{$word2}{$doc}{$positions2{$p2}}) {
				next;
			}
			while ($p1 < $n - 1 && $positions1[$p1 + 1] <= $positions2[$p2]) {
				$p1++;
			}
			if ($positions1[$p1] <= $positions2[$p2] and $positions2[$p2] - $positions1[$p1] <= $near and $inverted_index{$word1}{$doc}{$positions1{$p1}} == $inverted_index{$word2}{$doc}{$positions2{$p2}}) {
				$valid = 1;
				last;
			}
		}
		if ($valid == 1) {
			push @result, $doc;
		}
	}
	return @result;	
}

# main subroutine
sub main {
	open($input_file, '<', 'korpusD.txt');

	# input to useable data structures
	$doc_number = 0;
	$in_title = 0;
	$in_text = 1;
	$position = 0;
	$inverted_index = ();
	$inverted_index_pos = ();
	while ($row = <$input_file>) {
		chomp $row;
		# for windows platform
		if ($row =~ /\r$/) {
			chop $row;
		}

		if ($row eq "<DOC>") {
			$doc_number++;
			$position = 0;
		}

		if ($row =~ /^<TITLE>/) {
			foreach $word (split(/[^a-zA-Z0-9-]/, substr($row, 7, -8))) {
				if ($word =~ /^[a-zA-Z-]+$/) {
					$word = preprocess($word);
					if ($word =~/^[a-zA-Z]+$/) {
						$position++;
						$inverted_index{$word}{$doc_number}{$in_title} = 1;
						$inverted_index_pos{$word}{$doc_number}{$position} = 1;
					}
				}
			}
		}

		if ($row eq "<TEXT>") {
			$is_text = 1;
		} elsif ($row eq "</TEXT>") {
			$is_text = 0;
		} elsif ($is_text == 1) {
			foreach $word (split(/[^a-zA-Z0-9-]/, $row)) {
				if ($word =~ /^[a-zA-Z-]+$/) {
					$word = preprocess($word);
					if ($word =~/^[a-zA-Z]+$/) {
						$position++;
						$inverted_index{$word}{$doc_number}{$in_text} = 1;
						$inverted_index_pos{$word}{$doc_number}{$position} = 2;
					}
				}
			}
		}
	}

	# number 1 and 2 (output inverted index and inverted_index with zone)
	open($output_file1, '>', 'hasil_jawaban_ke1.txt');
	open($output_file2, '>', 'hasil_jawaban_ke2.txt');
	for $word (sort keys %inverted_index) {
		my @documents = sort {$a <=> $b} keys %{$inverted_index{$word}};
		print $output_file1 "$word ->";
		print $output_file2 "$word ->";
		for $document (@documents) {
			print $output_file1 " $document";
			if ($inverted_index{$word}{$document}{$in_title} and $inverted_index{$word}{$document}{$in_text}) {
				print $output_file2 " $document.title,$document.text";
			} elsif ($inverted_index{$word}{$document}{$in_title}) {
				print $output_file2 " $document.title";
			} else {
				print $output_file2 " $document.text";
			}
		}
		print $output_file1 "\n";
		print $output_file2 "\n";
	}
	close $output_file1;
	close $output_file2;

	# number 3 (output results of queries)
	$pertunjukan = preprocess("pertunjukan");
	$barongsai = preprocess("barongsai");
	$arsenal = preprocess("arsenal");
	$manchester = preprocess("manchester");
	$united = preprocess("united");

	@has_pertunjukan = get_docs($pertunjukan);
	@has_barongsai = get_docs($barongsai);
	@has_arsenal = get_docs($arsenal);
	@has_manchester = get_docs($manchester);
	@has_united = get_docs($united);
	@no_united = get_not_docs($united);
	@has_arsenal_and_manchester = boolean_and(\@has_arsenal, \@has_manchester);

	@query1_keys = ($pertunjukan, $barongsai);
	@query2_keys = ($arsenal, $manchester, $united);
	@query3_keys = ($arsenal, $manchester);

	@query1 = boolean_or(\@has_pertunjukan, \@has_barongsai);
	@query2 = boolean_and(\@has_arsenal_and_manchester, \@has_united);
	@query3 = boolean_and(\@has_arsenal_and_manchester, \@no_united);
	
	%result_query1 = score(\@query1, \@query1_keys);
	%result_query2 = score(\@query2, \@query2_keys);
	%result_query3 = score(\@query3, \@query3_keys);

	open($output_file, '>', 'hasil_jawaban_ke3a.txt');
	for $doc (sort { $result_query1{$b} <=> $result_query1{$a} } keys %result_query1) {
		print $output_file "Dokumen $doc skor: $result_query1{$doc}\n";
	}
	close $output_file;

	open($output_file, '>', 'hasil_jawaban_ke3b.txt');
	for $doc (sort { $result_query2{$b} <=> $result_query2{$a} } keys %result_query2) {
		print $output_file "Dokumen $doc skor: $result_query2{$doc}\n";
	}
	close $output_file;

	open($output_file, '>', 'hasil_jawaban_ke3c.txt');
	for $doc (sort { $result_query3{$b} <=> $result_query3{$a} } keys %result_query3) {
		print $output_file "Dokumen $doc skor: $result_query3{$doc}\n";
	}
	close $output_file;

	# number 4 (output inverted index with positions)
	open($output_file, '>', 'hasil_jawaban_ke4.txt');
	for $word (sort keys %inverted_index) {
		my @documents = sort {$a <=> $b} keys %{$inverted_index_pos{$word}};
		print $output_file "$word ->";
		for $document (@documents) {
			my @positions = sort {$a <=> $b} keys %{$inverted_index_pos{$word}{$document}};
			$freq = @positions;
			print $output_file " D$document\[$freq:";
			for $position (@positions) {
				print $output_file " $position";
			}
			print $output_file "]";
		}
		print $output_file "\n";
	}
	close $output_file;

	# number 5 (output result of queries)
	@result_query1 = boolean_near(preprocess("pasar"), preprocess("tradisional"), 1);
	@result_query2 = boolean_near(preprocess("Henry"), preprocess("gol"), 4);
	@result_query3 = boolean_near_plus_with(preprocess("polisi"), preprocess("jaksa"), preprocess("agung"), 3);

	open($output_file, '>', 'hasil_jawaban_ke5a.txt');
	for $doc (sort {$a <=> $b} @result_query1) {
		print $output_file "$doc\n";
	}
	close($output_file);

	open($output_file, '>', 'hasil_jawaban_ke5b.txt');
	for $doc (sort {$a <=> $b} @result_query2) {
		print $output_file "$doc\n";
	}
	close($output_file);

	open($output_file, '>', 'hasil_jawaban_ke5c.txt');
	for $doc (sort {$a <=> $b} @result_query3) {
		print $output_file "$doc\n";
	}
	close($output_file);
}

main();