package Stemmer;
require Exporter;
@ISA = qw(Exporter);
@EXPORT= qw(stem);

sub is_vowel {
	($char) = @_;
	if ($char =~ /^[aeiou]/) {
		return 1;
	}
	return 0;
}

sub subs {
	($word, $prefix_length, $suffix_length) = @_;
	$result = substr $word, $prefix_length, -$suffix_length;

	# content sensitive rules check
	if (length($result) < 4) {
		return $word;
	}

	return $result;
}

sub remove_inflexional_particle {
	($word) = @_;
	@affixes = ('kah', 'lah', 'tah', 'pun');

	foreach $affix (@affixes) {
		if ($word =~ /$affix$/) {
			return subs($word, 0, length($affix));
		}
	}


	return $word;
}

sub remove_inflexional_possesive {
	($word) = @_;
	@affixes = ('ku', 'mu', 'nya');
	foreach $affix (@affixes) {
		if ($word =~ /$affix$/) {
			return subs($word, 0, length($affix));
		}
	}

	return $word;
}

sub replace {
	($word, $from, $to, $replacement) = @_;
	$left = substr $word, 0, $from - 1;
	if ($from == 0) {
		$left = '';
	}
	$right = substr $word, $to;
	return $left . $replacement . $right;
}

sub stem {
	($word) = @_;
	# remove repeatance
	if ($word =~ /[a-z]-[a-z]/) {
		$pos = index $word, '-';
		$word = substr $word, 0, $pos;
	}

	# remove inflexionals
	$word = remove_inflexional_particle($word);
	$word = remove_inflexional_possesive($word);

	# remove suffix
	@suffixes = ('i', 'kan', 'an');
	$used_suffix = '';
	foreach $suffix (@suffixes) {
		if ($word =~ /$suffix$/) {
			$used_suffix = $suffix;
			$word = subs($word, 0, length($suffix));
			last;
		}
	}

	# remove prefix
	$counter = 0;
	$used_prefix = {};
	while ($counter < 3) {
		# constraint sensitive rule check
		if (length($word) <= 5) {
			last;
		}

		if ($word =~ /^me/) {
			if ($used_suffix eq 'an' or $used_prefix{'me'}) {
				last;
			}
			$used_prefix{'me'} = 1;

			if ($word =~ /^mem/) {
				if (is_vowel(substr ($word, 3, 1))) {
					$word = replace($word, 0, 3, 'p');
				} else {
					$word = replace($word, 0, 3, '');
				}
				$counter++;
			} elsif ($word =~ /^meny/) {
				if (is_vowel(substr ($word, 4, 1))) {
					$word = replace($word, 0, 4, 's');
				} else {
					$word = replace($word, 0, 4, '');
				}
			} elsif ($word =~ /^meng/) {
				if (is_vowel(substr ($word, 4, 1))) {
					$word = replace($word, 0, 4, 'k');
				} else {
					$word = replace($word, 0, 4, '');
				}
			} elsif ($word =~ /^men/) {
				if (is_vowel(substr($word, 3, 1))) {
					$word = replace($word, 0, 3, 't');
				} else {
					$word = replace($word, 0, 3, '');
				}
			} else {
				$word = replace($word, 0, 2, '');
			}
		} elsif ($word =~ /^be/) {
			if ($used_suffix eq 'i' or $used_prefix{'be'}) {
				last;
			}
			if ($word =~ /^belajar/) {
				$word = replace($word, 0, 3, '');
				$used_prefix{'be'} = 1;
				$counter++;
			} elsif ($word =~ /^be.er/ and is_vowel(substr($word, 5, 1)) == 0) {
				$word = replace($word, 0, 2, '');
				$used_prefix{'be'} = 1;
				$counter++;
			} elsif ($word =~ /^ber/) {

				$word = replace($word, 0, 3, '');
				$used_prefix{'be'} = 1;
				$counter++;
			} else {
				last;
			}
		} elsif ($word =~ /^di/) {
			if ($used_suffix eq 'an' or $used_prefix{'di'}) {
				last;
			}
			$word = replace($word, 0, 2, '');
			$counter++;
			$used_prefix{'di'} = 1;
		} elsif ($word =~ /^ke/) {
			if ($used_suffix eq 'i' or $used_suffix eq 'kan' or $used_prefix{'ke'}) {
				last;
			}
			$word = replace($word, 0, 2, '');
			$counter++;
			$used_prefix{'ke'} = 1;
		} elsif ($word =~ /^se/) {
			if ($used_suffix eq 'i' or $used_suffix eq 'kan' or $used_prefix{'se'}) {
				last;
			}
			$word = replace($word, 0, 2, '');
			$counter++;
			$used_prefix{'se'} = 1;
		} elsif ($word =~ /^ter/) {
			if ($used_suffix eq 'an' or $used_prefix{'ter'}) {
				last;
			}
			$word = replace($word, 0, 3, '');
			$counter++;
			$used_prefix{'ter'} = 1;
		} elsif ($word =~ /^pe/) {
			if ($used_prefix{'pe'}) {
				last;
			}
			if ($word =~ /^pelajar/) {
				$word = replace($word, 0, 3, '');
			} elsif ($word =~ /^peng/) {
				if (is_vowel(substr ($word, 4, 1))) {
					$word = replace($word, 0, 4, 'k');
				} else {
					$word = replace($word, 0, 4, '');
				}
			} elsif ($word =~ /^per/ and is_vowel(substr($word, 3, 1)) == 0) {
				$word = replace($word, 0, 3, '');
			} elsif ($word =~ /^pem/) {
				if (is_vowel(substr ($word, 3, 1))) {
					$word = replace($word, 0, 3, 'p');
				} else {
					$word = replace($word, 0, 3, '');
				}
				$counter++;
			} elsif ($word =~ /^peny/) {
				if (is_vowel(substr ($word, 4, 1))) {
					$word = replace($word, 0, 4, 's');
				} else {
					$word = replace($word, 0, 4, '');
				}
			} elsif ($word =~ /^pen/) {
				if (is_vowel(substr($word, 3, 1))) {
					$word = replace($word, 0, 3, 't');
				} else {
					$word = replace($word, 0, 3, '');
				}
			} else {
				$word = replace($word, 0, 2, '');
			}
			$counter++;
			$used_prefix{'pe'} = 1;
		}
		else {
			last;
		}
	}

	for $pref (keys %used_prefix) {
		$used_prefix{$pref} = 0;
	}

	return $word;
}
1;